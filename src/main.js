import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import dict from '@/assets/dict/dict'

createApp(App).use(store).use(router).use(dict).mount('#app')
