export default {
    validateNumber(txt) { return regex.number.test(txt) }
}

const regex = {
    number: /^[0-9]$/,
}