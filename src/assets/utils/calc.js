export default {
    percent(num) { return num / 100 },
    monthly(num) { return num / 1200 },
    month(year) { return year * 12 },
    year(month) { return Math.floor(month / 12) },
    compound(rate, period) { return Math.pow(1 + rate, period) },
    growth(initial, rate, period) { return initial * this.compound(rate, period) },
    grow(initial, rate) { return initial * (1+rate) },
    discount(initial, rate) { return initial * (1-rate) }
}