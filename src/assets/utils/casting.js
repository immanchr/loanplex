export default {
    toNumber(txt, float = false) {
        const type = typeof txt
        if (type === Number) {
            return txt
        }
        if (type === String) {
            return float ? parseFloat(txt) : parseInt(txt)
        }
        return 0
    }
}