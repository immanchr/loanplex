import EnglishDict from './en.json'

const i18n = {
    dictionary: {
        en: EnglishDict,
    },
    current: 'en',
    setLang(lang) {
        lang = Object.keys(this.dictionary).includes(lang) ? lang : 'en'
        this.current = lang
        window.localStorage.language = lang
    },
}

const dict = {
    get(key, ...data) {
        if (key == null || key.length == 0) {
            return ''
        }
        const dict = i18n.dictionary[i18n.current]
        if (dict == null) {
            return '!@#$ dictionary missing !@#$'
        }
        let result = dict[key]
        if (result == null) {
            return '!@#$' + key + '!@#$'
        }
        for (let i = 0; i < data.length; i++) {
            const substr = data[i]
            const template = '{' + i + '}'
            result = result.replace(template, substr)
        }
        return result
    }
}

const currency = {
    current: 'idr',
    currencies: {
        'usd': { name: 'Dollar', symbol: '$' },
        'idr': { name: 'Rupiah', symbol: 'Rp.'}
    },
    get() { return this.currencies[this.current].symbol }
}
export default {
    install: (app, options) => {
        app.config.globalProperties.$dict = dict.get
        app.provide('dict', options)
        app.config.globalProperties.$i18n = i18n
        app.provide('i18n', options)
        app.config.globalProperties.$currency = currency
        app.provide('currency', options)
    }
}