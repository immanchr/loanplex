import Investment from './investment';
import Loan from './loan';
import Property from './property';
import Salary from './salary';

export default class Scenario {
    constructor() {
        this.property = new Property()
        this.loan = new Loan()
        this.salary = new Salary()
        this.Investment = new Investment()
    }
}