import Casting from '@/assets/utils/casting'
import Calc from '@/assets/utils/calc'
import Interest from './interest'

export default class Loan {
    constructor() {
        this._downPayment = 0
        this._interests = []
    }
    copy() {
        const copy = new Loan()
        copy._downPayment = this._downPayment
        copy._interests = this._interests.slice()
        return copy
    }
    /**
     * @param {number} value 
     */
    set downPayment(value) { this._downPayment = Casting.toNumber(value, true) }
    get downPayment() { return this._downPayment }
    get downPaymentPercent() { return Calc.percent(this._downPayment) }

    get interests() { return this._interests }

    getInterest(index) { return this._interests[index] }
    setInterest({index, data}) { this._interests[index] = data }
    addInterest() {
        console.log('triggered')
        this._interests.push(new Interest())
    }
    deleteInterest(index) { this._interests.splice(index, 1) }

    get periodTotal() { return this.interests.reduce((sum, entry) => sum + entry.year) }

}