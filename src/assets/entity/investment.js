import Casting from '@/assets/utils/casting'
import Calc from '@/assets/utils/calc'

export default class Investment {
    constructor() {
        this._initial = 0
        this._growth = 0
        this.forDownPayment = true
    }
    copy() {
        const copy = new Investment()
        copy._initial = this._initial
        copy._growth = this._growth
        copy.forDownPayment = this.forDownPayment
        return copy
    }
     /**
     * @param {number} value
     */
    set initial(value) { this._initial = Casting.toNumber(value) }
    get initial() { return this._initial }
    /**
     * @param {number} value
     */
     set growth(value) { this._growth = Casting.toNumber(value, true) }
     get growth() { return this._growth }
     get growthMonthly() { return Calc.monthly(this._growth) }
}
