import Casting from '@/assets/utils/casting'
import Calc from '@/assets/utils/calc'

export default class Salary {
    constructor() {
        this._initial = 0
        this._growth = 0
    }
    copy() {
        const copy = new Salary()
        copy._initial = this._initial
        copy._growth = this._growth
        return copy
    }
    /**
     * @param {number} value
     */
    set initial(value) { this._initial = Casting.toNumber(value) }
    get initial() { return this._initial }
    /**
     * @param {number} value
     */
     set growth(value) { this._growth = Casting.toNumber(value, true) }
     get growth() { return this._growth }
     get growthPercent() { return Calc.percent(this._growth) }
}