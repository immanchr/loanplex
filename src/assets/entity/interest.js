import Casting from '@/assets/utils/casting'
import Calc from '@/assets/utils/calc'

export default class Interest {
    constructor() {
        this._rate = 0
        this._year = 0
    }
    copy() {
        const copy = new Interest()
        copy._rate = this._rate
        copy._year = this._year
        return copy
    }
    /**
     * @param {number} value
     */
     set rate(value) { this._rate = Casting.toNumber(value, true) }
     get rate() { return this._rate }
     get rateMonthly() { return Calc.monthly(this._rateMonthly) }
    /**
     * @param {number} value
     */
     set year(value) { this._year = Casting.toNumber(value) }
     get year() { return this._year }
     get month() { return Calc.month(this._year) }
}