import Casting from '@/assets/utils/casting'
import Calc from '@/assets/utils/calc'

export default class Property {
    constructor() {
        this._price = 0
        this._discount = 0
        this._growth = 0
    }
    copy() {
        const copy = new Property()
        copy._price = this._price
        copy._growth = this._growth
        copy._discount = this._discount
        return copy
    }
    /**
     * @param {number} value
     */
     set price(value) { this._price = Casting.toNumber(value) }
     get price() { return this._price }
     get priceDiscounted() { return Calc.discount(this._price, this.discountPercent) }
    /**
     * @param {number} value
     */
     set discount(value) { this._discount = Casting.toNumber(value, true) }
     get discount() { return this._discount }
     get discountPercent() { return Calc.percent(this._discount) }
     /**
     * @param {number} value
     */
     set growth(value) { this._growth = Casting.toNumber(value, true) }
     get growth() { return this._growth }
     get growthPercent() { return Calc.percent(this._growth) }
}