import Calc from '../../utils/calc'

export default class Installment {
    constructor(
        nthPeriod,
        income,
        incomeGrowth,
        principalPrevious,
        principalPayment,
        interestRate,
        investmentPrevious,
        investmentGrowth,
        propertyPricePrevious,
        propertyGrowth
    ) {
        // period
        this._nthPeriod = nthPeriod
        const year = Calc.year(nthPeriod)
        // principal
        this._principalPayment = principalPayment
        this._principalRemaining = principalPrevious - principalPayment
        // interest
        this._interestPayment = this._principalRemaining * interestRate
        this._installment = principalPayment + this._interestPayment

        // investment
        this._income = Calc.growth(income, incomeGrowth, year)
        this._investment = this._income - this._installment
        const investmentGrown = Calc.growth(investmentPrevious, investmentGrowth, nthPeriod)
        this._investmentTotal = investmentGrown + this._investment
        // property
        this._propertyPrice = Calc.grow(propertyPricePrevious, propertyGrowth)
        // most important, asset
        this._assetTotal = this._investmentTotal + this._propertyPrice
    }
    get nthPeriod() { return this._nthPeriod }
    get principalPayment() { return this._principalPayment }
    get principalRemaining() { return this._principalRemaining }
    get interestPayment() { return this._interestPayment }
    get installment() { return this.installment }
    get income() { return this._income }
    get investment() { return this._investment }
    get investmentTotal() { return this._investmentTotal }
    get propertyPrice() { return this._propertyPrice }
    get assetTotal() { return this._assetTotal }

}