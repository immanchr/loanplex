import Calc from '../../utils/calc'
import Installment from './installment'

export default class PaymentTable {
    constructor(
        principalInitial,
        interestRate,
        interestPeriod,
        periodInitial,
        periodTotal,
        salaryInitial,
        salaryGrowth,
        investmentPrevious,
        investmentGrowth,
        propertyPrice,
        propertyGrowth,
    ) {
        const initialPrincipalPayment = this.calcInitialPrincipalPayment(principalInitial, interestRate, periodTotal)
        const paymentTable = this.generateInstallments(
            periodInitial,
            principalInitial,
            initialPrincipalPayment,
            interestRate,
            interestPeriod,
            salaryInitial,
            salaryGrowth,
            investmentPrevious,
            investmentGrowth,
            propertyPrice,
            propertyGrowth,
        )
        this._installments = paymentTable.installments
        this._principalInitial = principalInitial
        this._principalRemaining = paymentTable.principalRemaining
        this._principalPayment = this._principalInitial - this._principalRemaining
        this._interestTotal = paymentTable.interestTotal
        this._paymentTotal = paymentTable.paymentTotal
    }

    get installments() { return this._installments }
    get principalInitial() { return this._principalInitial }
    get principalRemaining() { return this._principalRemaining }
    get principalPayment() { return this._principalPayment }
    get interestTotal() { return this._interestTotal }
    get paymentTotal() { return this._paymentTotal }
    get investmentTotal() { return this.lastInstallment().investmentTotal }
    get assetTotal() { return this.lastInstallment().assetTotal }

    lastInstallment() { return this._installments[this._installments.length - 1] }

    calcInitialPrincipalPayment(totalPrincipal, interestRate, totalPeriod) {
        const divisor = Calc.compound(interestRate, totalPeriod) - 1
        return totalPrincipal * interestRate / divisor
    }

    generateInstallments(
        periodInitial,
        principalInitial,
        initialPrincipalPayment,
        interestRate,
        interestPeriod,
        salaryInitial,
        salaryGrowth,
        investmentPrevious,
        investmentGrowth,
        propertyPrice,
        propertyGrowth,
    ) {
        const result = []
        let paymentTotal = 0
        let interestTotal = 0
        let principalPrev = principalInitial
        let principalPayment = initialPrincipalPayment
        for (let i = 0; i < interestPeriod; i++) {
            const installment = new Installment(
                periodInitial + i,
                principalPrev,
                principalPayment,
                interestRate,
                salaryInitial,
                salaryGrowth,
                investmentPrevious,
                investmentGrowth,
                propertyPrice,
                propertyGrowth,
            )
            result.push(installment)
            paymentTotal += installment.installment
            interestTotal += installment.interestPayment
            principalPrev = installment.remainingPrincipal
            principalPayment = Calc.grow(principalPrev, interestRate)
        }
        return {
            installments: result,
            principalRemaining: principalPrev,
            paymentTotal,
            interestTotal
        }
    }
}