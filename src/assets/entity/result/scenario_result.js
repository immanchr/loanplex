import PaymentTable from './payment_table'

export default class ScenarioResult {
    constructor(scenario) {
        this._scenario = scenario
        this._propertyPriceDiscounted = scenario.property.priceDiscounted
        this._downPayment = this._propertyPriceDiscounted * scenario.loan.downPaymentPercent
        this._principalInitial = this._propertyPriceDiscounted - this._downPayment
        this._investmentInitial = scenario.investment.initial - (scenario.investment.forDownPayment ? this._downPayment : 0)
        this._periodTotal = this.scenario.loan.periodTotal
        this._paymentTables = this.generatePaymentTables(
            scenario.loan.interests,
            this._principalInitial,
            this._periodTotal,
            scenario.salary,
            this._investmentInitial,
            scenario.investment,
            scenario.property
        )
    }
    
    get scenario() { return this._scenario }
    get propertyPriceDiscounted() { return this._propertyPriceDiscounted }
    get downPayment() { return this._downPayment }
    get principalInitial() { return this._principalInitial }
    get investmentInitial() { return this._investmentInitial }
    get periodTotal() { return this._periodTotal }
    get paymentTables() { return this._paymentTables }

    /**
     * @param {Array} interests
     * @param {Number} principalInitial
     * @param {Number} periodTotal
     * @param {Object} salary
     * @param {Number} investmentInitial
     * @param {Object} investmentGrowth
     * @param {Object} property 
     */
    generatePaymentTables(
        interests,
        principalInitial,
        periodTotal,
        salary,
        investmentInitial,
        investment,
        property,
    ) {
        const result = []
        let periodPrev = 0
        let principalPrev = principalInitial
        let investmentPrev = investmentInitial
        for (let i = 0; i < interests.length; i ++) {
            const interest = interests[i]
            const table = new PaymentTable(
                principalPrev, // principalInitial
                interest.rateMonthly, // interestRate
                interest.month, // interestPeriod
                periodPrev, // periodInitial
                periodTotal, // periodTotal
                salary.initial, // salaryInitial
                salary.growthPercent, // salaryGrowth
                investmentPrev, // investmentPrevious
                investment.growthMonthly, // investmentGrowth
                property.price, // propertyPrice
                property.growthPercent, // propertyGrowth
            )
            result.push(table)
            periodPrev += interest.month
            principalPrev = table.principalRemaining
            investmentPrev = table.investmentTotal
        }
        return result
    }
}